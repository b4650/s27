const http = require("http");

let database = [
{
	"name": "Brandon",
	"email": "brandon@mail.com"
},
{
	"name": "Jobert",
	"email": "jobert@mail.com"
}
]

http.createServer((req, res)=>{
	if(req.url === "/users" && req.method === "GET") {
		res.writeHead(200, {"Content-Type": "_application/json"});
/*
Server to client > Stringify (data will be received by users/client from the server will be in a form of stringified JSON)
client to server > Parse
*/
		res.write(JSON.stringify(database));
		res.end()
	}
	

	if(req.url === "/users" && req.method === "POST") {
		let requestBody = ""
		req.on("data", function(data){
			requestBody += data
		})
		req.on("end", function(){
			requestBody = JSON.parse(requestBody)

			let newUser = {
				"name" : requestBody.name,
				"email" : requestBody.email
			}
			database.push(newUser);
			console.log(database);

			res.writeHead(200, {"Content-Type": "_application/json"});
			res.write(JSON.stringify(newUser));
			res.end();
		})
	}
}).listen(3000)

console.log("Server is running at localhost: 3000")