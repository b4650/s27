const http = require("http");
const port = 4000;

const server = http.createServer((req, res)=>{
	if (req.url === "/item" && req.method === "GET"){
		res.writeHead(200, {"Content-Type": "text/plain"})
		res.end("Data retrieved from database")
	}
	if (req.url === "/item" && req.method === "POST"){
		res.writeHead(200, {"Content-Type": "text/plain"})
		res.end("Data to be sent to database")
	}
	if (req.url === "/item" && req.method === "PUT"){
		res.writeHead(200, {"Content-Type": "text/plain"})
		res.end("Update resources")
	}
	if (req.url === "/item" && req.method === "DELETE"){
		res.writeHead(200, {"Content-Type": "text/plain"})
		res.end("Delete resources")
	}
});

server.listen(port);

console.log(`Server is running at localhost: ${port}`)